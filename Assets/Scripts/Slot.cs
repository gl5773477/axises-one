using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
    SlotData slotData;
    public GameObject ballObj;

    //槽的坐标:(所在的圆，当前圆的索引顺序),位置索引从Vector.right开始，逆时针计数
    int circle, idx;
    public int Circle { get => circle; }
    public int Idx { get => idx; }

    public void Init(int x, int y, SlotData data)
    {
        circle = x;
        idx = y;
        slotData = data;
    }

    public SlotData GetSlotData()
    {
        return this.slotData;
    }

    //绑定一个球
    public void BindBall(Ball ball)
    {
        ballObj = ball.gameObject;
    }
}
