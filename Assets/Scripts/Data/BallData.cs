﻿using System;
public class BallData
{
    public int number;  //球上的编号
    public int index;   //在整个求盘上的索引
    public int circle;  //位于第几圈
    public float rad;   //所在的弧度
    public float angle; //角度
}
