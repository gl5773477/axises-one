﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// 一个圈，用一个对象来保存以方便操作
/// </summary>
public class Circle
{
    //当前圈的层级
    int level;

    //当前圈槽的间隔度数
    float intervalAngle;

    #region ***************************** property ****************************

    public float IntervalAngle { get => intervalAngle; }
    public int Level { get => level; set => level = value; }

    #endregion

    //圈中所包含的槽, key为槽编号
    Dictionary<int, Slot> slotDic = new Dictionary<int, Slot>();

    public Circle(int level, float intervalAngle)
    {
        this.level = level;
        this.intervalAngle = intervalAngle;
    }

    #region *****************************公共方法*****************************

    public void AddSlot(Slot slot)
    {
        slotDic.Add(slot.Idx, slot);
    }

    public Slot GetSlotByIdx(int idx)
    {
        return slotDic[idx];
    }

    public int GetSlotCount()
    {
        return slotDic.Count;
    }

    /// <summary>
    /// 寻找同方向的空槽
    /// </summary>
    /// <param name="compareTragetObj">用于比较的目标对象</param>
    /// <returns></returns>
    public Slot GetEmptySlotWithSameDirection(GameObject compareTragetObj)
    {
        for (int i = 0; i < slotDic.Count; i++)
        {
            float targetAngle = UtilAngle.Angle360(Vector3.right, compareTragetObj.transform.position.normalized);
            float slotAngle = UtilAngle.Angle360(Vector3.right, slotDic[i].transform.position.normalized);
            //如果角度差值<间隔角度的一半，则认为相近
            if (Mathf.Abs(slotAngle - targetAngle) < intervalAngle / 2)
            {
                Debug.LogFormat("找到同方向槽编号{0}, 其所在的角度{1}, 圈{2}, 其上的球码为{3}", slotDic[i].Idx, slotDic[i].GetSlotData().angle, level, slotDic[i].ballObj ? slotDic[i].ballObj.GetComponent<Ball>()._Number : -1);
                if (slotDic[i].ballObj == null)
                {
                    return slotDic[i];
                }
                return null;
            }
            else
            {
                //Debug.LogFormat("槽编号{0}, 槽上球号码{1}, 当前槽所在度数{2}, 飞行球位置度数{3}, 度数差值{4}", j, slotDic[i].ballObj ? slotDic[i].ballObj.GetComponent<Ball>()._Number : -1, slotDic[i].GetSlotData().angle, spinAngle, slotAngle - spinAngle);
            }
        }
        return null;
    }

    #endregion

}