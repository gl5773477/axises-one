using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject _Slot;
    public GameObject _Game;
    public List<GameObject> _BallList;
    public Ball _SpinBall;
    public int _SlotCircle;

    //间隔角度
    readonly float intervalAngle = 60;
    //基础圆半径
    readonly float radius = 1.3f;
    //按圈存储的槽和球
    readonly Dictionary<int, List<GameObject>> allSlotDic = new Dictionary<int, List<GameObject>>();
    readonly Dictionary<int, List<GameObject>> allBallDic = new Dictionary<int, List<GameObject>>();

    /// <summary>
    /// 所有环词典：每个环维护一个槽列表
    /// </summary>
    readonly Dictionary<int, Circle> circleDic = new Dictionary<int, Circle>();

    List<int> ballIndexs = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
    void Start()
    {
        InitMap();
        _SpinBall.ChangeSpinState(true);
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            //_SpinBall.ChangeSpinState(false);
            //_SpinBall.ChangeFlyState(true);
            FlyToTargetSlot();
        }
    }

    void InitMap()
    {
        int curIndex = 0;
        int circleLevel = 0;
        int curSlotCountOfCircle = (circleLevel > 0 ? 6 : 1);
        float curIntervalAngle = intervalAngle;

        while (circleLevel <= _SlotCircle)
        {
            Circle circle = new Circle(circleLevel, curIntervalAngle);
            circleDic.Add(circleLevel, circle);

            List<GameObject> ballList = new List<GameObject>();
            List<GameObject> slotList = new List<GameObject>();
            for (int i = 0; i < curSlotCountOfCircle; i++)
            {
                Slot slot = genSlotByRad(circleLevel, i, curIntervalAngle);
                slotList.Add(slot.gameObject);
                circle.AddSlot(slot);

                //随机抛弃，后面可替换算法决策
                if (circleLevel == 0 || Random.Range(0, 2) > 0)
                {
                    //在槽上生成一个ball
                    Ball ball = genBallWithSlot(slot.gameObject);
                    ballList.Add(ball.gameObject);

                    BallData data = new BallData();
                    data.index = curIndex;
                    data.circle = circleLevel;
                    data.rad = slot.GetSlotData().rad;
                    data.angle = slot.GetSlotData().angle;
                    ball.Init(data);
                    //Debug.LogFormat("圈{0},当前球编号{1}，角度{2}, i={3}", curCircle, ball._Number, data.angle, i);

                    // 槽和球关联
                    slot.ballObj = ball.gameObject;

                    //球计数索引
                    curIndex++;
                }
            }

            allBallDic.Add(circleLevel, ballList);
            allSlotDic.Add(circleLevel, slotList);

            circleLevel++;
            curSlotCountOfCircle = circleLevel * 6;
            curIntervalAngle = intervalAngle / (curSlotCountOfCircle / 6);
        }
    }

    /// <summary>
    /// 按弧度生成一个槽
    /// </summary>
    /// <param name="circle">所在圈层</param>
    /// <param name="idx">在圈中的序号</param>
    /// <param name="intervalAngle">当前间隔角度</param>
    Slot genSlotByRad(int circle, int idx, float intervalAngle)
    {
        float rad = idx * intervalAngle * Mathf.Deg2Rad;
        //Debug.LogFormat("槽的弧度：{0}", rad);
        GameObject slotObj = Instantiate(_Slot);
        float x = Mathf.Cos(rad) * circle * radius;
        float y = Mathf.Sin(rad) * circle * radius;
        slotObj.transform.position = new Vector3(x, y, 0);
        slotObj.transform.SetParent(_Game.transform);
        Slot slot = slotObj.GetComponent<Slot>();
        if (slot)
        {
            SlotData data = new SlotData();
            data.circle = circle;
            data.rad = rad;
            data.angle = idx * intervalAngle;
            slot.Init(circle, idx, data);
            //Debug.LogFormat("当前槽circle={0}, 角度{1}", circleLevel, posAngle);
        }
        return slot;
    }

    /// <summary>
    /// 在槽的位置上生成一个球
    /// </summary>
    /// <param name="slot">槽</param>
    /// <returns></returns>
    Ball genBallWithSlot(GameObject slot)
    {
        // 随机选择一种球
        int rdIndex = Random.Range(0, _BallList.Count);
        GameObject ballObj = Instantiate(_BallList[rdIndex]);
        if (ballObj)
        {
            ballObj.transform.position = slot.transform.position;
            ballObj.transform.SetParent(_Game.transform);
        }
        return ballObj.GetComponent<Ball>();
    }

    /// <summary>
    /// 飞向槽
    /// 1、如果当前飞向的方向，槽有空位置（除圆心槽），则直接飞到空位置后再触发消除
    /// 2、如果当前飞向的方向，槽无空位置，则直接废弃（暂不处理）
    /// </summary>
    /// <returns></returns>
    void FlyToTargetSlot()
    {
        float spinAngle = UtilAngle.Angle360(Vector3.right, _SpinBall.transform.position.normalized);

        //最内圈寻找同方向的空槽
        int curCircleLevel = circleDic.Count - 1;
        Circle cicle = circleDic[curCircleLevel];

        Slot emptyslot = cicle.GetEmptySlotWithSameDirection(_SpinBall.gameObject);
        if (emptyslot == null)
        {
            //直接丢弃，重新生成
            _SpinBall.ChangeSpinState(false);
            _SpinBall.Disappear();
            //生成新球
            Ball newBall = genBallWithSlot(_SpinBall.gameObject);
            newBall.ChangeSpinState(true);
            _SpinBall = newBall;
            return;
        }
        else
        {
            Ball newBall = genBallWithSlot(_SpinBall.gameObject);
            newBall.ChangeSpinState(true);
            //飞向空槽
            _SpinBall.FlyToSlot(emptyslot);
            emptyslot.BindBall(_SpinBall);
            //重新标记飞行球
            _SpinBall = newBall;
        }
    }
}
