﻿using System;
using UnityEngine;

public class UtilAngle
{
    public UtilAngle()
    {
    }

    /// <summary>
    /// 计算向量的夹角，0~360度
    /// </summary>
    /// <param name="from_"></param>
    /// <param name="to_"></param>
    /// <returns></returns>
    public static float Angle360(Vector3 from_, Vector3 to_)
    {
        Vector3 v3 = Vector3.Cross(from_, to_);
        if (v3.z > 0)
            return Vector3.Angle(from_, to_);
        else
            return 360 - Vector3.Angle(from_, to_);
    }
}