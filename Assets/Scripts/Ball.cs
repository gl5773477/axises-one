using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour
{
    public int _Number;

    BallData ballData;
    float spinSpeed = 60.0f;
    bool isSpining = false;
    float flySpeed = 10.0f;
    bool isFlying = false;
    Vector3 direction;

    void Start()
    {

    }

    void Update()
    {
        if (isSpining)
            SpinAroundInCircle();
    }

    public void Init(BallData data)
    {
        this.ballData = data;
        this.ballData.number = _Number;
    }

    public BallData GetBallData()
    {
        return this.ballData;
    }

    public void SpinAroundInCircle()
    {
        transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 0, 1), spinSpeed * Time.deltaTime);
    }

    public void ChangeSpinState(bool state)
    {
        isSpining = state;
    }

    public void ChangeSpinSpeed(float value)
    {
        spinSpeed = value;
    }

    public void FlyToSlot(Slot slot)
    {

        //this.transform.position = Vector3.Lerp(this.transform.position-targetObj.transform.position, 0.5f);
        //Vector3 direction = this.transform.position - targetObj.transform.position;

        //transform.Translate(direction * Time.deltaTime * flySpeed, Space.World);
        //if (transform.position == Vector3.zero)
        //    ChangeFlyState(false);

        transform.position = slot.transform.position;
        ChangeSpinState(false);
    }

    public void ChangeFlyState(bool state)
    {
        transform.DOMove(Vector3.zero, 1.0f);
    }

    public void Disappear()
    {
        ballData = null;
        Destroy(this.gameObject);
    }

}
